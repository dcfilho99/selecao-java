package com.br.teste.indra;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InitTesteIndraApplication {
	
	@PostConstruct
    public void init(){
        TimeZone.setDefault(TimeZone.getTimeZone("UTC-3")); 
    }

	public static void main(String[] args) {
		SpringApplication.run(InitTesteIndraApplication.class, args);
	}

}
