package com.br.teste.indra.repository.projection;

public interface MediaPrecoCombustivel {

	String getMunicipio();
	String getProduto();
	double getMediaPreco();
}
