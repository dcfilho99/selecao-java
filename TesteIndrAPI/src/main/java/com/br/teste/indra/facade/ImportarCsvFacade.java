package com.br.teste.indra.facade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.factory.HistoricoCombustivelFactory;
import com.br.teste.indra.service.HistoricoCombustivelService;
import com.br.teste.indra.util.ArquivoUtil;

@Component
public class ImportarCsvFacade {
	
	@Autowired
	private HistoricoCombustivelService service;
	
	@Autowired
	private ImportaRegiaoCsvFacade regiaoFacade;
	
	@Autowired
	private ImportaEstadoCsvFacade estadoFacade;
	
	@Autowired
	private ImportaLocalCsvFacade localFacade;
	
	@Autowired
	private ImportaProdutoCsvFacade produtoFacade;
	
	@Autowired
	private ImportaRevendaCsvFacade revendaFacade;
	
	public void converter(MultipartFile file) throws Exception {
		if (!file.getOriginalFilename().toLowerCase().contains(".csv")) {
			throw new Exception("Arquivo inválido");
		}
		
		List<HistoricoCombustivel> retorno = new ArrayList<>();
		try {
			List<String> linhas = ArquivoUtil.getInstance().lerArquivo(file);			
			int i = 0;
            for (String linha : linhas) {
        		if (i > 0) {
	            	List<String> colunas = Arrays.asList(linha.split("\t"));
	            	if (colunas.size() == 11) {
	            		HistoricoCombustivel historico = HistoricoCombustivelFactory.newInstanceFromCsv(colunas);
	            		retorno.add(historico);
	            	}
	            }
				i++;
            }
            regiaoFacade.iniciarRegioes(retorno);
            estadoFacade.iniciarEstados(retorno);
            localFacade.iniciarLocais(retorno);
            produtoFacade.iniciarProdutos(retorno);
            revendaFacade.iniciarRevendas(retorno);
			
            service.saveAll(retorno);
            retorno.clear();
        } catch (IOException ex1) {
            System.out.println("Erro lendo arquivo. " + ex1.getLocalizedMessage());
        }
	}
}
