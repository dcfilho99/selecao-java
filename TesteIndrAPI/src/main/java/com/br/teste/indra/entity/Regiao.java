package com.br.teste.indra.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@Entity
@Table(name = "regiao")
public class Regiao implements Serializable {


	private static final long serialVersionUID = 7425908927170614294L;
	
	@Id
	@GeneratedValue(generator = "regiao_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "regiao_id_seq", sequenceName = "regiao_id_seq")
	private Long id;
	
	@NotEmpty
	@Column(name = "sigla", length = 2)
	private String sigla;
}
