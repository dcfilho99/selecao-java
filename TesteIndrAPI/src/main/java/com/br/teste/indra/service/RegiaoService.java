package com.br.teste.indra.service;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.teste.indra.entity.Regiao;
import com.br.teste.indra.repository.RegiaoRepository;

@Service
public class RegiaoService extends EntityService<Regiao> {

	@Autowired
	private RegiaoRepository repository;
	
	@PostConstruct
	public void init() {
		setRepository(repository);
	}
	
	public Optional<Regiao> findBySigla(String sigla) {
		return repository.findBySigla(sigla);
	}
}
