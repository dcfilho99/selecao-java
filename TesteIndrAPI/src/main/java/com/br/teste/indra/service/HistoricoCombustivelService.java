package com.br.teste.indra.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.repository.HistoricoCombustivelRepository;
import com.br.teste.indra.repository.projection.MediaPrecoCombustivel;
import com.br.teste.indra.repository.projection.ValorMedioBandeira;
import com.br.teste.indra.repository.projection.ValorMedioMunicipio;

@Service
public class HistoricoCombustivelService extends EntityService<HistoricoCombustivel> {

	@Autowired
	private HistoricoCombustivelRepository repository;
	
	@PostConstruct
	public void init() {
		setRepository(repository);
	}
	
	public List<HistoricoCombustivel> getAll() {
		return repository.findAll();
	}
	
	public Page<HistoricoCombustivel> getByRegiaoSigla(String regiaoSigla, Pageable pageable) {
		if (regiaoSigla == null) {
			regiaoSigla = "";
		}
		return repository.findByLocalEstadoRegiaoSiglaContainingIgnoreCase(regiaoSigla, pageable);
	}
	
	public Page<MediaPrecoCombustivel> mediaPrecoPorMunicipio(String municipio, Pageable pageable) {
		return repository.getMediaValorCompraPorMunicipio(municipio, pageable);
	}
	
	public Page<ValorMedioMunicipio> valoresMedioPorMunicipio(String municipio, Pageable pageable) {
		return repository.getValorMedioCompraVendaPorMunicipio(municipio, pageable);
	}

	public Page<ValorMedioBandeira> valoresMedioPorBandeira(String bandeira, Pageable pageable) {
		return repository.getValorMedioCompraVendaPorBandeira(bandeira, pageable);
	}

	public List<HistoricoCombustivel> pesquisar() {
		return repository.findAll(PageRequest.of(1, 100)).getContent();
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
}
