package com.br.teste.indra.endpoint;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.br.teste.indra.entity.Usuario;
import com.br.teste.indra.exception.ResourceNotFoundException;
import com.br.teste.indra.service.UsuarioService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("v1/usuarios")
public class UsuarioEndpoint {
	
	@Autowired
	private UsuarioService service;
	
	@GetMapping(path = "/{id}")
	@ApiOperation(
		value = "Busca o usuário pelo ID",
		notes = "Requisição GET passando o ID do usuário à ser retornado",
		response = Usuario.class
	)	
	public ResponseEntity<?> getById(@PathVariable Long id) {
		Optional<Usuario> opt = service.findById(id);
		if (!opt.isPresent()) {
			throw new ResourceNotFoundException("Usuário não encontrado");
		}
		return new ResponseEntity<>(opt.get(), HttpStatus.OK);
	}
	
	@ApiOperation(
		value = "Pesquisa de usuários",
		notes = "Requisição GET passando um objeto para filtrar valores à serem retornados",
		response = Usuario[].class
		
	)	
	@GetMapping(path = "/pesquisa")
	public ResponseEntity<?> pesquisar(@RequestParam(name = "nome", required = false) String nome) {
		return new ResponseEntity<>(service.pesquisar(nome), HttpStatus.OK);
	}
	
	@PostMapping
	@ApiOperation(
			value = "Insere um usuário",
			notes = "Requisição POST passando um Usuario para salvar um novo registro",
			response = Usuario.class
		)
	public ResponseEntity<?> save(@RequestBody @Valid Usuario usuario) {
		return new ResponseEntity<>(service.save(usuario), HttpStatus.OK);
	}
	
	@PutMapping
	@ApiOperation(
			value = "Atualiza informações de um usuário",
			notes = "Requisição PUT passando um Usuario para atualizar o registro",
			response = Usuario.class
		)
	public ResponseEntity<?> update(@RequestBody Usuario usuario) {
		return new ResponseEntity<>(service.save(usuario), HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/{id}")
	@ApiOperation(
			value = "Exclui um usuário",
			notes = "Requisição DELETE passando o ID do usuário à ser excluído"
		)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
