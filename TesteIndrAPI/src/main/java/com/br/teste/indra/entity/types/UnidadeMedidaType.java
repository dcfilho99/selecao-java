package com.br.teste.indra.entity.types;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
@JsonFormat(shape = Shape.OBJECT)
public enum UnidadeMedidaType {

	RS_LITRO(1, "R$/litro"),;
	
	private Integer value;
	private String description;
	
	private UnidadeMedidaType(Integer value, String description) {
		this.value = value;
		this.description = description;
	}
	
	public static UnidadeMedidaType fromDescription(String description) {
		for (UnidadeMedidaType type : UnidadeMedidaType.values()) {
			if (type.getDescription().toUpperCase().equals(description.toUpperCase())) {
				return type;
			}
		}
		return null;
	}
	
	@JsonCreator
	public static UnidadeMedidaType jsonCreator(@JsonProperty("value") Integer value, @JsonProperty("description") String description) {
		return Arrays.asList(UnidadeMedidaType.values()).stream()
						.filter( item -> 
							item.getValue().equals(value) && 
							item.getDescription().equals(description)
						).findFirst().get();
	}
}
