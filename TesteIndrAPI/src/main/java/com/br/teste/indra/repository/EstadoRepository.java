package com.br.teste.indra.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.teste.indra.entity.Estado;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long> {

	Optional<Estado> findBySigla(String sigla);
	List<Estado> findByRegiaoSigla(String sigla);
}
