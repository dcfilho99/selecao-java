package com.br.teste.indra.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.teste.indra.entity.Estado;
import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.service.EstadoService;

@Component
class ImportaEstadoCsvFacade {
	
	@Autowired
	private EstadoService estadoService;	

	public void iniciarEstados(List<HistoricoCombustivel> retorno) {
		List<Estado> estados = new ArrayList<>();
		
		Set<Estado> produtosAsStr = (Set<Estado>) retorno.stream()
										.map(item -> item.getLocal().getEstado())
										.collect(Collectors.toSet());
		
		for (Estado prd : produtosAsStr) {
			Optional<Estado> opt = estadoService.findBySigla(prd.getSigla());
			if (opt.isPresent()) {
				prd = opt.get();
			}	
			estados.add(prd);
		}
		estados = estadoService.saveAll(estados);
		
		for (HistoricoCombustivel hist : retorno) {
			String sigla = hist.getLocal().getEstado().getSigla();
			
			Estado est = estados.stream()
							.filter(
								item -> item.getSigla().toUpperCase().equals(sigla.toUpperCase())
							).findFirst().get();
			
			hist.getLocal().setEstado(est);
		}
	}
}
