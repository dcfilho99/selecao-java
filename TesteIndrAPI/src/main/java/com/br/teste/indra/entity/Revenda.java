package com.br.teste.indra.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@Entity
@Table(name = "revenda")
public class Revenda implements Serializable {

	private static final long serialVersionUID = -7169068849788277842L;

	@Id
	@GeneratedValue(generator = "revenda_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "revenda_id_seq", sequenceName = "revenda_id_seq")
	private Long id;
	
	@NotEmpty
	@Column(name = "nome")
	private String nome;
	
	@NotEmpty
	@Column(name = "cnpj")
	private String cnpj;
	
	@NotEmpty
	private String bandeira;
	
}
