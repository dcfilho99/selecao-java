package com.br.teste.indra.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.teste.indra.entity.Local;

@Repository
public interface LocalRepository extends JpaRepository<Local, Long> {
	
	public List<Local> findByEstadoSigla(String estadoSigla);
	
	public Optional<Local> findByMunicipioAndEstadoSigla(String nome, String estadoSigla);

	public Local findByEstadoRegiaoSiglaAndEstadoSiglaAndMunicipio(String regiaoSigla, String estadoSigla, String municipio);
	
	public boolean existsByEstadoRegiaoSiglaIgnoreCaseAndEstadoSiglaIgnoreCaseAndMunicipioIgnoreCase(String regialSigla, 
																						String estado, 
																						String municipio);
}
