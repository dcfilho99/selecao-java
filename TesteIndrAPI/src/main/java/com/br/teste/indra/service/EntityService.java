package com.br.teste.indra.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract class EntityService<T extends Serializable> {

	private JpaRepository<T, Long> repository;
	
	protected void setRepository(JpaRepository<T, Long> repository) {
		this.repository = repository;
	}
	
	public Optional<T> findById(Long id) {
		return repository.findById(id);
	}
	
	public List<T> findAll() {
		return repository.findAll();
	}
	
	public Page<T> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}
	
	public T save(T clazz) {
		return repository.save(clazz);
	}
	
	public List<T> saveAll(List<T> clazz) {
		return repository.saveAll(clazz);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
}
