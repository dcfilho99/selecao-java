package com.br.teste.indra.repository.projection;

import java.math.BigDecimal;

public interface ValorMedioMunicipio {

	String getMunicipio();
	String getProduto();
	BigDecimal getVlrVenda();
	BigDecimal getVlrCompra();
}
