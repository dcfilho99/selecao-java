package com.br.teste.indra.service;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.teste.indra.entity.Produto;
import com.br.teste.indra.repository.ProdutoRepository;

@Service
public class ProdutoService extends EntityService<Produto> {

	@Autowired
	private ProdutoRepository repository;
	
	@PostConstruct
	public void init() {
		setRepository(repository);
	}
	
	public Optional<Produto> findByNome(String nome) {
		return repository.findByNome(nome);
	}
}
