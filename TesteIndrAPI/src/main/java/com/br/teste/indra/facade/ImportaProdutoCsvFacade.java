package com.br.teste.indra.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.entity.Produto;
import com.br.teste.indra.service.ProdutoService;

@Component
class ImportaProdutoCsvFacade {
	
	@Autowired
	private ProdutoService produtoService;

	public void iniciarProdutos(List<HistoricoCombustivel> retorno) {
		List<Produto> produtos = new ArrayList<>();
		
		Set<Produto> produtosAsStr = (Set<Produto>) retorno.stream()
										.map(item -> item.getProduto())
										.collect(Collectors.toSet());
		
		for (Produto prd : produtosAsStr) {
			Optional<Produto> opt = produtoService.findByNome(prd.getNome());
			if (opt.isPresent()) {
				prd = opt.get();
			}
			produtos.add(prd);
		}
		produtos = produtoService.saveAll(produtos);
		
		for (HistoricoCombustivel hist : retorno) {
			String produto = hist.getProduto().getNome();
			
			Produto prod = produtos.stream()
							.filter(
								item -> item.getNome().toUpperCase().equals(produto.toUpperCase())
							).findFirst().get();
			
			hist.setProduto(prod);
		}
	}
}
