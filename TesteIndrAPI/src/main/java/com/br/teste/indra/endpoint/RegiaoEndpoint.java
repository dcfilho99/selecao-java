package com.br.teste.indra.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.teste.indra.entity.Regiao;
import com.br.teste.indra.service.RegiaoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("v1/regioes")
public class RegiaoEndpoint {
	
	@Autowired
	private RegiaoService service;
	
	@GetMapping
	@ApiOperation(
		value = "Busca todas as regiões",
		notes = "Requisição GET para retornar todas as regiões",
		response = Regiao[].class
	)	
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
	}
}
