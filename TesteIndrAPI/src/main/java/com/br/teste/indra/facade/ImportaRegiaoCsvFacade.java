package com.br.teste.indra.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.entity.Regiao;
import com.br.teste.indra.service.RegiaoService;

@Component
class ImportaRegiaoCsvFacade {
	
	@Autowired
	private RegiaoService regiaoService;

	public void iniciarRegioes(List<HistoricoCombustivel> retorno) {
		List<Regiao> regioes = new ArrayList<>();
		
		Set<String> produtosAsStr = (Set<String>) retorno.stream()
										.map(item -> item.getLocal().getEstado().getRegiao().getSigla())
										.collect(Collectors.toSet());
		
		for (String prd : produtosAsStr) {
			Optional<Regiao> opt = regiaoService.findBySigla(prd);
			if (!opt.isPresent()) {
				regioes.add(new Regiao(null, prd));
			} else {
				regioes.add(opt.get());
			}
			
		}
		regioes = regiaoService.saveAll(regioes);
		
		for (HistoricoCombustivel hist : retorno) {
			String sigla = hist.getLocal().getEstado().getRegiao().getSigla();
			
			Regiao reg = regioes.stream()
							.filter(
								item -> item.getSigla().toUpperCase().equals(sigla.toUpperCase())
							).findFirst().get();
			
			hist.getLocal().getEstado().setRegiao(reg);
		}
	}
}
