package com.br.teste.indra.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@Entity
@Table(name = "estado")
public class Estado implements Serializable {
	
	private static final long serialVersionUID = 6944601238666701592L;

	@Id
	@GeneratedValue(generator = "estado_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "estado_id_seq", sequenceName = "estado_id_seq")
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "regiao_id")
	private Regiao regiao;
	
	@NotEmpty
	@Column(name = "sigla", length = 2)
	private String sigla;
}
