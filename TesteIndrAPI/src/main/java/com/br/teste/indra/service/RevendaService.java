package com.br.teste.indra.service;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.teste.indra.entity.Revenda;
import com.br.teste.indra.repository.RevendaRepository;

@Service
public class RevendaService extends EntityService<Revenda> {

	@Autowired
	private RevendaRepository repository;
	
	@PostConstruct
	public void init() {
		setRepository(repository);
	}
	
	public Optional<Revenda> findByCnpj(String cnpj) {
		return repository.findByCnpj(cnpj);
	}
}
