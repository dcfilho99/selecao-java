package com.br.teste.indra.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.entity.Revenda;
import com.br.teste.indra.service.RevendaService;

@Component
class ImportaRevendaCsvFacade {
	
	@Autowired
	private RevendaService revendaService;

	public void iniciarRevendas(List<HistoricoCombustivel> retorno) {
		List<HistoricoCombustivel> novoRetorno = new ArrayList<>();
		
		Map<Revenda, List<HistoricoCombustivel>> mapHist = retorno.stream()
																.collect(Collectors.groupingBy(HistoricoCombustivel::getRevenda));
		
		for (Revenda revenda : mapHist.keySet()) {
			List<HistoricoCombustivel> historicos = mapHist.get(revenda);
			
			Optional<Revenda> opt = revendaService.findByCnpj(revenda.getCnpj());
			if (opt.isPresent()) {
				revenda = opt.get();
			} else {
				revenda = revendaService.save(revenda);
			}			
			final Revenda rev = revenda;
			historicos.stream().forEach(item -> item.setRevenda(rev));
			
			novoRetorno.addAll(historicos);
		}
		
		retorno = novoRetorno;
	}
}
