package com.br.teste.indra.endpoint;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.br.teste.indra.error.ErroDetails;
import com.br.teste.indra.error.ResourceNotFoundDetails;
import com.br.teste.indra.error.ValidationErrorDetails;
import com.br.teste.indra.exception.ResourceNotFoundException;

@ControllerAdvice
public class EndpointExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = ResourceNotFoundException.class)
	public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException rnfException) {
		ResourceNotFoundDetails details = ResourceNotFoundDetails.Builder
									.newBuilder()
									.timestamp(new Date().getTime())
									.status(HttpStatus.NOT_FOUND.value())
									.title("Resource not found")
									.detail(rnfException.getMessage())
									.developerMessage(rnfException.getClass().getName())
									.build();
		
		return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException rnfException, HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		List<FieldError> fields = rnfException.getBindingResult().getFieldErrors();
		
		ValidationErrorDetails details = ValidationErrorDetails.Builder
				.newBuilder()
				.timestamp(new Date().getTime())
				.status(HttpStatus.BAD_REQUEST.value())
				.title("Field Validation Error")
				.detail("Field Validation Error")
				.developerMessage(rnfException.getClass().getName())
				.fieldErrors(fields)
				.build();

		return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
	}
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(
			Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {

		ErroDetails er = ErroDetails.builder()
				.timestamp(new Date().getTime())
				.status(status.value())
				.title("Internal exception")
				.detail(ex.getMessage())
				.developerMessage(ex.getClass().getName())
				.build();

		return new ResponseEntity<Object>(er, headers, status);	
	}
}
