package com.br.teste.indra.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.entity.Local;
import com.br.teste.indra.service.LocalService;

@Component
class ImportaLocalCsvFacade {

	@Autowired
	private LocalService localService;
	
	public void iniciarLocais(List<HistoricoCombustivel> retorno) {
		List<Local> locais = new ArrayList<Local>();
		
		Set<Local> locaisAsStr = (Set<Local>) retorno.stream()
										.map(item -> item.getLocal())
										.collect(Collectors.toSet());
		
		for (Local l : locaisAsStr) {
			Optional<Local> opt = localService.findByMunicipioAndEstadoSigla(l.getMunicipio(), l.getEstado().getSigla());
			if (opt.isPresent()) {
				l = opt.get();
			}
			locais.add(l);
		}
		locais = localService.saveAll(locais);		
		
		for (HistoricoCombustivel hist : retorno) {
			String municipio = hist.getLocal().getMunicipio();
			
			Local local = locais.stream()
							.filter(
								item -> item.getMunicipio().toUpperCase().equals(municipio.toUpperCase())
							).findFirst().get();
			
			hist.setLocal(local);
		}
	}
}
