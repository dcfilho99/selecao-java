package com.br.teste.indra.service;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.teste.indra.entity.Local;
import com.br.teste.indra.repository.LocalRepository;

@Service
public class LocalService extends EntityService<Local> {

	@Autowired
	private LocalRepository repository;
	
	@PostConstruct
	public void init() {
		setRepository(repository);
	}
	
	public Optional<Local> findByMunicipioAndEstadoSigla(String municipio, String estadoSigla) {
		return repository.findByMunicipioAndEstadoSigla(municipio, estadoSigla);
	}
	
	public List<Local> findByEstadoSigla(String estadoSigla) {
		return repository.findByEstadoSigla(estadoSigla);
	}
}
