package com.br.teste.indra.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.teste.indra.entity.Usuario;
import com.br.teste.indra.repository.UsuarioRepository;

@Service
public class UsuarioService extends EntityService<Usuario>{

	@Autowired
	private UsuarioRepository repository;
	
	@PostConstruct
	public void init() {
		setRepository(repository);
	}
	
	public List<Usuario> pesquisar(String nome) {
		if (nome == null) {
			nome = "";
		}
		return repository.findByNomeContainingIgnoreCaseOrderByNomeAsc(nome);
	}
	
	public Usuario save(Usuario usuario) {
		return repository.save(usuario);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
}
