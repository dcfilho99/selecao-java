package com.br.teste.indra.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class ArquivoUtil {

	private static ArquivoUtil instance;
	
	private ArquivoUtil() {
		
	}
	
	public List<String> lerArquivo(MultipartFile file) throws UnsupportedEncodingException, IOException {
		BufferedReader ler = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-16"));
		
        String linha;
        List<String> linhas = new ArrayList<String>();
        while (ler.ready()) {
        		linha = ler.readLine();
        		linhas.add(linha);
        }
        ler.close();
        return linhas;
	}
	
	public static ArquivoUtil getInstance() {
		if (instance == null) {
			instance = new ArquivoUtil();
		}
		return instance;
	}
}
