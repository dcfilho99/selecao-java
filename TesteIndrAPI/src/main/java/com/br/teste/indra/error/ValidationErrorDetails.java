package com.br.teste.indra.error;

import java.util.List;

import org.springframework.validation.FieldError;

public class ValidationErrorDetails extends ErroDetails {

	private List<FieldError> fieldErrors;
	
	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldError> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	private ValidationErrorDetails() {

	}

	public static final class Builder {
		private String title;
		private int status;
		private String detail;
		private long timestamp;
		private String developerMessage;
		private List<FieldError> fieldErrors;

		private Builder() {

		}

		public static Builder newBuilder() {
			return new Builder();
		}

		public Builder title(String title) {
			this.title = title;
			return this;
		}

		public Builder status(int status) {
			this.status = status;
			return this;
		}

		public Builder detail(String detail) {
			this.detail = detail;
			return this;
		}

		public Builder timestamp(long timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public Builder developerMessage(String devMessage) {
			this.developerMessage = devMessage;
			return this;
		}
		
		public Builder fieldErrors(List<FieldError> fieldErrors) {
			this.fieldErrors = fieldErrors;
			return this;
		}

		public ValidationErrorDetails build() {
			ValidationErrorDetails d = new ValidationErrorDetails();
			d.setTitle(title);
			d.setStatus(status);
			d.setDetail(detail);
			d.setTimestamp(timestamp);
			d.setDeveloperMessage(developerMessage);
			d.setFieldErrors(fieldErrors);
			return d;
		}
	}
}
