import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { HistoricoCombustivelListComponent } from './historico-combustivel-list/historico-combustivel-list.component';
import { HistoricoCombustivelFormComponent } from './historico-combustivel-form/historico-combustivel-form.component';
import { HistoricoCombustivelMediaPrecoComponent } from "./hist-comb-media-preco/hist-com-media-preco.component";
import { HistoricoCombustivelValoresComponent } from "./hist-comb-valores/hist-comb-valores.component";
import { HistoricoCombustivelDistribuidoraComponent } from './hist-comb-distribuidora/hist-comb-distribuidora.component';
import { HistoricoCombustivelDataColetaComponent } from './hist-comb-data-coleta/hist-comb-data-coleta.component';

import { NgxCurrencyModule } from "ngx-currency";


import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt'

import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        NgxCurrencyModule,
        NgxPaginationModule
    ],
    declarations: [
        HistoricoCombustivelListComponent,
        HistoricoCombustivelFormComponent,
        HistoricoCombustivelMediaPrecoComponent,
        HistoricoCombustivelValoresComponent,
        HistoricoCombustivelDistribuidoraComponent,
        HistoricoCombustivelDataColetaComponent
    ]
})
export class HistoricoCombustivelModule { }