import { Component, OnInit } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { ErrorService } from '../../../services/error.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { RegiaoService } from '../../../services/regiao.service';
import { ProdutoService } from '../../../services/produto.service';
import { EstadoService } from '../../../services/estado.service';
import { LocalService } from '../../../services/local.service';
import { Regiao } from '../../../domain/regiao';
import { Estado } from '../../../domain/estado';
import { Local } from '../../../domain/local';
import { Produto } from '../../../domain/produto';
import { Revenda } from '../../../domain/revenda';
import { Enum } from '../../../domain/enum';
import { HistoricoCombustivel } from '../../../domain/historico.combustivel';
import { UnidadeMedidaService } from '../../../services/unidade.medida.service';
import { RevendaService } from '../../../services/revenda.service';

@Component({ 
    selector: 'historico-combustivel-form', 
    templateUrl: 'historico-combustivel-form.component.html' 
})
export class HistoricoCombustivelFormComponent implements OnInit {

    regioes: Regiao[] = [];
    estados: Estado[] = [];
    locais: Local[] = [];
    produtos: Produto[] = [];
    revendas: Revenda[] = [];
    unidadesMedida: Enum[] = [];

    form = this.formBuilder.group({
        id: [null, []],
        dtColeta: ['', Validators.required],
        regiaoSigla: ['', [Validators.required]],
        estadoSigla: ['', Validators.required],        
        municipio: ['', [Validators.required]],
        revenda: ['', [Validators.required]],
        produto: ['', [Validators.required]],
        vlrCompra: ['', [Validators.required]],
        vlrVenda: ['', [Validators.required]],
        unidadeMedida: ['', [Validators.required]],
    });

    historico: HistoricoCombustivel = new HistoricoCombustivel();

    confSenha: string;

    editing: Boolean = false;

    constructor(private service: HistoricoCombustivelService,
                private route: ActivatedRoute,
                private router: Router,
                private formBuilder: FormBuilder,
                private httpError: ErrorService,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService,
                private regiaoService: RegiaoService,
                private estadoService: EstadoService,
                private produtoService: ProdutoService,
                private localService: LocalService,
                private unidadeMedidaService: UnidadeMedidaService,
                private revendaService: RevendaService) {
        let id = this.route.snapshot.params.id;
        if (id) {
            this.initEstados();
            this.editing = true;
            this.spinner.show();
            this.service.get(id).subscribe(res => {
                this.historico = <HistoricoCombustivel>res.body;
                this.revendas.push(this.historico.revenda);
                setTimeout(() => {
                    this.setForm();
                }, 50);
                setTimeout(() => {
                    this.loadMunicipios();
                }, 75);
                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.httpError.errorHandler(error);
            });
        } else {
            this.loadRevendas();
        }
    }

    ngOnInit() {
        this.loadRegioes();
        this.loadProdutos();
        this.loadUnidadesMedida();
    }

    save() {
        if (this.editing) {
            this.update();
        } else {
            this.setHistoricoComsutivel();
            this.spinner.show();
            this.service.save(this.historico).subscribe(res => {
                this.historico = Object.assign(this.historico, res.body);
                this.spinner.hide();
                this.router.navigate(['/historico-combustiveis']);
            }, error => {
                this.spinner.hide();
                this.httpError.errorHandler(error);
            });
        }
    }

    update() {
        this.setHistoricoComsutivel();
        this.spinner.show();
        this.service.update(this.historico).subscribe(res => {
            this.historico = Object.assign(this.historico, res.body);
            this.spinner.hide();
            this.router.navigate(['/historico-combustiveis']);
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    setHistoricoComsutivel() {
        let produto: Produto = <Produto>this.form.value.produto;
        let revenda: Revenda = <Revenda>this.form.value.revenda;
        let local: Local = <Local>this.form.value.municipio;
        let unidadeMedida: Enum = <Enum>this.form.value.unidadeMedida;
        
        this.historico = new HistoricoCombustivel();
        this.historico.local = local;
        this.historico.revenda = revenda;
        this.historico.produto = produto;
        this.historico.unidadeMedida = unidadeMedida;
        this.historico.vlrCompra = this.form.value.vlrCompra;
        this.historico.vlrVenda = this.form.value.vlrVenda;
        this.historico.dtColeta = new Date();
    }

    setForm() {
        this.form = this.formBuilder.group({
            id: [null, []],
            dtColeta: [formatDate(this.historico.dtColeta, 'yyyy-MM-dd', 'pt-BR', 'UTC-3'), Validators.required],
            regiaoSigla: [this.historico.local.estado.regiao, [Validators.required]],
            estadoSigla: [ this.historico.local.estado, Validators.required],        
            municipio: [this.historico.local, [Validators.required]],
            revenda: [this.historico.revenda, [Validators.required]],
            produto: [this.historico.produto, [Validators.required]],
            vlrCompra: [this.historico.vlrCompra, [Validators.required]],
            vlrVenda: [this.historico.vlrVenda, [Validators.required]],
            unidadeMedida: [this.historico.unidadeMedida, [Validators.required]],
        });
        console.log(this.form.value);
    }

    loadRegioes() {
        this.spinner.show();
        this.regiaoService.getAll().subscribe(res => {
            this.regioes = <Array<Regiao>>res.body;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    loadEstados() {
        this.spinner.show();
        this.estadoService.getByRegiao(this.form.value.regiaoSigla.sigla).subscribe((res) => {
            this.estados = <Array<Estado>>res.body;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    loadMunicipios() {
        this.spinner.show();
        this.localService.getByEstado(this.form.value.estadoSigla.sigla).subscribe((res) => {
            this.locais = <Array<Local>>res.body;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    loadProdutos() {
        this.spinner.show();
        this.produtoService.getAll().subscribe((res) => {
            this.produtos = <Array<Produto>>res.body;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    loadUnidadesMedida() {
        this.spinner.show();
        this.unidadeMedidaService.getAll().subscribe((res) => {
            this.unidadesMedida = <Array<Enum>>res.body;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    loadRevendas() {
        this.spinner.show();
        this.revendaService.getAll().subscribe((res) => {
            this.revendas = <Array<Revenda>>res.body;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    initEstados() {
        this.estadoService.getAll().subscribe((res) => {
            this.estados = <Array<Estado>>res.body;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }
}