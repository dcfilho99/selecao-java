import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HistoricoCombustivelListComponent } from './historico-combustivel-list/historico-combustivel-list.component';
import { HistoricoCombustivelFormComponent } from './historico-combustivel-form/historico-combustivel-form.component';


const routes: Routes = [
    { path: 'historico-combustiveis', component: HistoricoCombustivelListComponent },
    { path: 'historico-combustiveis/new', component: HistoricoCombustivelFormComponent },
    { path: 'historico-combustiveis/:id', component: HistoricoCombustivelFormComponent }
];

@NgModule({
    exports: [ RouterModule ],
    imports: [ RouterModule.forRoot(routes) ]
  })
export class HistoricoCombustivelRoutingModule {}