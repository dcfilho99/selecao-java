import { Component } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { Router } from '@angular/router';
import { ErrorService } from '../../../services/error.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({ 
    selector: 'historico-combustivel-list', 
    templateUrl: 'historico-combustivel-list.component.html' 
})
export class HistoricoCombustivelListComponent {

    page = 1;

    sigla = ""

    rows:any = [];
    totalItens: number;

    tabs = ['pesquisa', 'mediaPreco', 'valores'];
    tabActive = 'pesquisa';

    constructor(private service: HistoricoCombustivelService,
                private router: Router,
                private httpError: ErrorService,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService) {

    }

    pesquisar() {
        this.spinner.show();
        this.service.pesquisar(this.sigla, (this.page-1)).subscribe(res => {
            this.rows = res.body;
            this.page = (this.rows.pageable.pageNumber+1);
            this.totalItens = this.rows.totalElements;
            this.rows = this.rows.content;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    nextPage() {
        this.page++;
        this.pesquisar();
    }

    previousPage() {
        this.page--;
        this.pesquisar();
    }

    edit(id: number) {
        this.router.navigate(['/historico-combustiveis/' + id]);
    }

    remove(id: number) {
        var conf = confirm("Tem certeza que deseja exlcuir o registro?");
        if (conf) {
            this.spinner.show();
            this.service.delete(id).subscribe(res => {
                this.spinner.hide();
                this.toastr.success("Registro excluído com sucesso");
                this.pesquisar();
            });
        }
    }

    setTabActive(name: string) {
        this.tabActive = name;
    }
}