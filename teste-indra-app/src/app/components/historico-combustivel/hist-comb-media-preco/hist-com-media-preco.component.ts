import { Component } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { Router } from '@angular/router';
import { ErrorService } from '../../../services/error.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({ 
    selector: 'hist-comb-media-preco', 
    templateUrl: 'hist-comb-media-preco.component.html' 
})
export class HistoricoCombustivelMediaPrecoComponent {

    municipio = ""

    page = 1;
    totalItens = 0;

    rows:any = [];

    constructor(private service: HistoricoCombustivelService,
                private router: Router,
                private httpError: ErrorService,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService) {

    }

    pesquisar() {
        this.spinner.show();
        this.service.getMediaPreco(this.municipio, (this.page-1)).subscribe(res => {
            this.rows = res.body;
            this.page = (this.rows.pageable.pageNumber+1);
            this.totalItens = this.rows.totalElements;
            this.rows = this.rows.content;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }
}