import { Component, OnInit } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { ErrorService } from '../../../services/error.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({ 
    selector: 'hist-comb-data-coleta', 
    templateUrl: 'hist-comb-data-coleta.component.html' 
})
export class HistoricoCombustivelDataColetaComponent implements OnInit {

    rows:any = [];

    page = 1;
    totalItens = 0;

    constructor(private service: HistoricoCombustivelService, 
                private httpError: ErrorService,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
        this.pesquisar();
    }

    pesquisar() {
        this.spinner.show();
        this.service.getAgrupadosDtColeta((this.page-1)).subscribe(res => {
            this.rows = res.body;
            this.page = (this.rows.pageable.pageNumber+1);
            this.totalItens = this.rows.totalElements;
            this.rows = this.rows.content;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }
}