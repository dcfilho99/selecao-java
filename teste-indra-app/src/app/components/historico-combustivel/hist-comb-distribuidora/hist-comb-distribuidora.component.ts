import { Component, OnInit } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { ErrorService } from '../../../services/error.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({ 
    selector: 'hist-comb-distribuidora', 
    templateUrl: 'hist-comb-distribuidora.component.html' 
})
export class HistoricoCombustivelDistribuidoraComponent implements OnInit {

    page = 1;
    totalItens = 0;

    rows:any = [];

    constructor(private service: HistoricoCombustivelService,
                private httpError: ErrorService,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
        this.pesquisar();
    }

    pesquisar() {
        this.spinner.show();
        this.service.getAgrupadosDistribuidora((this.page-1)).subscribe(res => {
            this.spinner.hide();
            this.rows = res.body;
            this.page = (this.rows.pageable.pageNumber+1);
            this.totalItens = this.rows.totalElements;
            this.rows = this.rows.content;
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }
}