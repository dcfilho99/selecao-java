import { Component } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { Router } from '@angular/router';
import { ErrorService } from '../../../services/error.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({ 
    selector: 'hist-comb-valores', 
    templateUrl: 'hist-comb-valores.component.html' 
})
export class HistoricoCombustivelValoresComponent {

    municipio = "";
    bandeira = "";

    page = 1;
    totalItens = 0;

    rowsBandeira:any = [];
    rowsMunicipio:any = [];

    tipoPesquisa = "municipio";

    constructor(private service: HistoricoCombustivelService,
                private router: Router,
                private httpError: ErrorService,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService) {

    }

    pesquisarBandeiras() {
        this.spinner.show();
        this.service.getValorMedioBandeira(this.bandeira, (this.page-1)).subscribe(res => {
            this.rowsBandeira = res.body;
            this.rowsBandeira = res.body;
            this.page = (this.rowsBandeira.pageable.pageNumber+1);
            this.totalItens = (this.rowsBandeira.totalElements);
            this.rowsBandeira = this.rowsBandeira.content;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    pesquisarMunicipios() {
        this.spinner.show();
        this.service.getValorMedioMunicipio(this.municipio, (this.page-1)).subscribe(res => {
            this.rowsMunicipio = res.body;
            this.page = (this.rowsMunicipio.pageable.pageNumber+1);
            this.totalItens = (this.rowsMunicipio.totalElements);
            this.rowsMunicipio = this.rowsMunicipio.content;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.httpError.errorHandler(error);
        });
    }

    reset() {
        this.rowsBandeira = [];
        this.rowsMunicipio = [];
        this.page = 1;
        this.totalItens = 0;
    }
}