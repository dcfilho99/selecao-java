import { Component } from '@angular/core';
import { HistoricoCombustivelService } from '../../services/historico.comb.serice';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { ErrorService } from '../../services/error.service';

@Component({ 
    selector: 'conversor', 
    templateUrl: 'conversor.component.html' 
})
export class ConversorComponent {

    fileToUpload: File;
    progress = 0;

    uploading = false;

    constructor(private service: HistoricoCombustivelService,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService,
                private errorhandler: ErrorService) {

    }

    upload() {
        this.uploading = true;
        this.service.uploadCsv(this.fileToUpload).subscribe((event: HttpEvent<any>) => {
            console.log(event);
            switch (event.type) {
              case HttpEventType.Sent:
                break;
              case HttpEventType.ResponseHeader:
                break;
              case HttpEventType.UploadProgress:
                this.progress = Math.round(event.loaded / event.total * 100);
                if (this.progress == 100) {
                  this.toastr.success("Arquivo enviado!");
                  setTimeout(() => {
                    this.toastr.warning("Estamos trabalhando na conversão dos dados do arquivo.");
                    setTimeout(() => {
                      this.toastr.warning("Isso pode levar alguns instantes!");
                    }, 2500);
                  }, 2500);                  
                  this.spinner.show(); 
                }
                break;
              case HttpEventType.Response:
                this.spinner.hide();
                this.toastr.success("Dados importados com sucesso");
                this.progress = 0;
                break;
            }
        }, error => {
          this.progress = 0;
          this.errorhandler.errorHandler(error);
        });
    }

    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
    }
}