import { Component } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({ 
    selector: 'usuario-list', 
    templateUrl: 'usuario-list.component.html' 
})
export class UsuarioListComponent {

    pesquisaObj = {
        nome: ""
    };

    rows:any = [];

    constructor(private usuarioService: UsuarioService,
                private router: Router,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService) {
    }

    pesquisar() {
        this.spinner.show();
        this.usuarioService.pesquisar(this.pesquisaObj.nome).subscribe(res => {
            this.rows = res.body;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.toastr.error(error);
        });
    }

    edit(id: number) {
        this.router.navigate(['/usuarios/' + id]);
    }

    remove(id: number) {
        var conf = confirm("Tem certeza que deseja exlcuir o registro?");
        if (conf) {
            this.spinner.show();
            this.usuarioService.delete(id).subscribe(res => {
                this.spinner.hide();
                this.toastr.success("Registro excluído com sucesso");
                this.pesquisar();
            }, error => {
                this.spinner.hide();
                this.toastr.error(error);
            });
        }
    }
}