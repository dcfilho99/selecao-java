import { Component } from '@angular/core';
import { Usuario } from '../../../domain/usuario';
import { UsuarioService } from '../../../services/usuario.service';

import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ErrorService } from '../../../services/error.service';

@Component({ 
    selector: 'usuario-form', 
    templateUrl: 'usuario-form.component.html' 
})
export class UsuarioFormComponent {

    form = this.formBuilder.group({
        nome: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        senha: ['', Validators.required],        
        confSenha: ['', [Validators.required]],
    });

    usuario = new Usuario();

    confSenha: string;

    editing: Boolean = false;

    constructor(private service: UsuarioService,
                private route: ActivatedRoute,
                private router: Router,
                private formBuilder: FormBuilder,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService,
                private errorHandler: ErrorService) {
        let id = this.route.snapshot.params.id;
        if (id) {
            this.form.get('senha').setValidators([]);
            this.form.get('confSenha').setValidators([]);
            this.editing = true;
            this.spinner.show();
            this.service.get(id).subscribe(res => {
                this.usuario = Object.assign(this.usuario, res.body);
                this.setForm();
                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.errorHandler.errorHandler(error);
            });
        }
    }

    save() {
        if (this.editing) {
            this.update();
        } else {
            this.setUsuario();
            this.spinner.show();
            this.service.save(this.usuario).subscribe(res => {
                this.spinner.hide();
                this.usuario = Object.assign(this.usuario, res.body);
                this.toastr.success("Registro gravado com sucesso");
                this.router.navigate(['/usuarios']);
            }, error => {
                this.spinner.hide();
                this.errorHandler.errorHandler(error);
            });
        }
    }

    update() {
        this.setUsuario();
        this.spinner.show();
        this.service.update(this.usuario).subscribe(res => {
            this.spinner.hide();
            this.usuario = Object.assign(this.usuario, res.body);
            this.toastr.success("Registro gravado com sucesso");
            this.router.navigate(['/usuarios']);
        }, error => {
            this.spinner.hide();
            this.errorHandler.errorHandler(error);
        });
    }

    setUsuario() {
        this.usuario.nome = this.form.value.nome;
        this.usuario.email = this.form.value.email;
        this.usuario.senha = this.form.value.senha;
    }

    setForm() {
        this.form.get("nome").setValue(this.usuario.nome);
        this.form.get("email").setValue(this.usuario.email);
        this.form.get("senha").setValue(this.usuario.senha);
    }

}