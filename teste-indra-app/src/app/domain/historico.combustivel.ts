import { Regiao } from "./regiao";
import { Local } from "./local";
import { Revenda } from "./revenda";
import { Produto } from "./produto";
import { Enum } from "./enum";

export class HistoricoCombustivel {
    id: number;
    local: Local;
    revenda: Revenda;
    produto: Produto;
    dtColeta: Date;
    vlrCompra: number;
    vlrVenda: number;
    unidadeMedida: Enum;
}