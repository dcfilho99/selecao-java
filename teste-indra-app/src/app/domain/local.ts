import { Estado } from "./estado";

export class Local {
    id: number;
    municipio: string;
    estado: Estado;
}