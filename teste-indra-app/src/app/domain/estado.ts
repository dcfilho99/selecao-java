import { Regiao } from "./regiao";

export class Estado {
    id: number;
    sigla: string;
    regiao: Regiao;
}