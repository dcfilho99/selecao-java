import { Injectable } from '@angular/core';
import { ApiService } from './api';
import { Usuario } from '../domain/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  endpoint = "/v1/usuarios";

  constructor(private api: ApiService) { 

  }

  pesquisar(nome) {
    var url = this.endpoint + "/pesquisa?nome=" + nome;
    return this.api.get(url);
  }

  save(usuario: Usuario) {
    return this.api.post(this.endpoint, usuario);
  }

  get(id: number) {
    var url = this.endpoint + "/" + id;
    return this.api.get(url);
  }

  update(usuario: Usuario) {
    return this.api.put(this.endpoint, usuario);
  }

  delete(id: number) {
    var url = this.endpoint + "/" + id;
    return this.api.delete(url);
  }
}