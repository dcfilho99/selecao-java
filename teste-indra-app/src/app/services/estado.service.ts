import { Injectable } from '@angular/core';
import { ApiService } from './api';
import { Usuario } from '../domain/usuario';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  endpoint = "/v1/estados";

  constructor(private api: ApiService) { 

  }

  getAll() {
    var url = this.endpoint;
    return this.api.get(url);
  }

  getByRegiao(regiao: string) {
    var url = this.endpoint + "/" + regiao;
    return this.api.get(url);
  }
}