import { Injectable } from '@angular/core';
import { ApiService } from './api';

@Injectable({
  providedIn: 'root'
})
export class RevendaService {

  endpoint = "/v1/revendas";

  constructor(private api: ApiService) { 

  }

  getAll() {
    var url = this.endpoint;
    return this.api.get(url);
  }
}