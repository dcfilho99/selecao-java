import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  SEM_CONEXAO = "Não foi possível estabelecer comunicação com o servidor.\nTente novamente";
  ERRO_INTERNO = "Erro no servidor.\nTente novamente";

  constructor(private toastr: ToastrService) { 

  }

  public errorHandler(error: HttpErrorResponse) {
      if (error.status === 0) {
          this.toastr.error(this.SEM_CONEXAO);
      } else if (error.status === 400) {
        let msg = "";
        let fields:any = error.error.fieldErrors;
        for (let i = 0; i < fields.length; i++) {
            msg = fields[i].field + " " + fields[i].defaultMessage;
            this.toastr.warning(msg);
            msg = null;
        }        
      } else if (error.status === 500) {
        this.toastr.error(this.ERRO_INTERNO);
      }
  }
}