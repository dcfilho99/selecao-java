import { Injectable } from '@angular/core';
import { ApiService } from './api';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  endpoint = "/v1/produtos";

  constructor(private api: ApiService) { 

  }

  getAll() {
    var url = this.endpoint;
    return this.api.get(url);
  }
}