import { Injectable } from '@angular/core';
import { ApiService } from './api';

@Injectable({
  providedIn: 'root'
})
export class UnidadeMedidaService {

  endpoint = "/v1/unidades-medida";

  constructor(private api: ApiService) { 

  }

  getAll() {
    var url = this.endpoint;
    return this.api.get(url);
  }
}