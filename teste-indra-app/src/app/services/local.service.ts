import { Injectable } from '@angular/core';
import { ApiService } from './api';
import { Usuario } from '../domain/usuario';

@Injectable({
  providedIn: 'root'
})
export class LocalService {

  endpoint = "/v1/locais";

  constructor(private api: ApiService) { 

  }

  getByEstado(estado: string) {
    var url = this.endpoint + "/" + estado;
    return this.api.get(url);
  }
}