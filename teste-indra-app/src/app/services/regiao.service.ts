import { Injectable } from '@angular/core';
import { ApiService } from './api';

@Injectable({
  providedIn: 'root'
})
export class RegiaoService {

  endpoint = "/v1/regioes";

  constructor(private api: ApiService) { 

  }

  getAll() {
    var url = this.endpoint;
    return this.api.get(url);
  }
}