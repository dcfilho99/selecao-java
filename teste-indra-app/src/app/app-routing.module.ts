import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConversorComponent } from './components/conversor/conversor.component';

const routes: Routes = [
  { path: 'conversor', component: ConversorComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule {}